FROM node:alpine

WORKDIR /usr/src/app
COPY package*.json ./
COPY src/ ./src
RUN npm install
EXPOSE 8080

ENTRYPOINT ["npm","start"]